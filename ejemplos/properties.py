"""Propiedades del buscador de archivos

Este archivo se compone de la siguiente manera:

CONSTANTES
----------
HOST: str
    La ruta sobre la cual se aplicarán las pruebas de carga.
HEADERS: dict
    Nombre del cliente registrado en el header de cada petición.
"""

HOST = 'https://jsonplaceholder.typicode.com/posts'
HEADERS = {'User-Agent': 'CuRT'}
DIRECTORY = 'C:/'
