<a name="curt"></a>
# CuRT

## Table of contents
1. [CuRT](#curt)
	1. [Introducción](#intro)
	2. [Requerimientos](#req)
	3. [Usa CuRT](#get)
	4. [Configuración](#conf)
	5. [Usando el código fuente](#source)
	2. [Mantenimiento](#main)

<a name="intro"></a>
## Introducción
[Custom RPS Tester](https://gitlab.com/jahaziel-alvarez/curt) es una herramienta para generar un número de solicitudes por segundo (RPS) específico a un servicio web.

<a name="req"></a>
## Requerimientos
- Versión de Python
    - Python 3


- Módulos
    - Pandas 1.5.2
    - Plotly 5.11.0
    - Requests 2.28.1
	
<a name="get"></a>
## Usa CuRT

```bash
pip install curt
```

<a name="conf"></a>
## Configuración
Los módulos de la herramienta CuRT están en la carpeta `src\curt`. Ahí es donde se encuentran todos los archivos necesarios para poder utilizar CuRT. Esto significa que no existe un archivo principal por defecto. El archivo CuRT.py es un ejemplo de un archivo de ejecución.

### Ejemplo
En la carpeta de ***ejemplos*** hay dos archivos. El archivo `properties.py` trae variables de ejemplo que pueden ser utilizadas desde un archivo de python que consuma la herramienta CuRT, por ejemplo, el *host* para pruebas `https://jsonplaceholder.typicode.com/posts`. El archivo `post.json` trae un objeto Json de ejemplo que puede ser utilizado para consumir la API de ejemplo.

Ya en el archivo principal, que en este ejemplo se llamará `CuRT.py`, se puede importar el archivo de `properties.py`:
```python
try:
    from resources import properties as p
except (ModuleNotFoundError, ImportError):
    print('Error: Propiedades no encontradas.')
```

En la sección de *imports* también puede ir incluida la herramienta CuRT:

```python
import src.curt.dictionary_creator as dc
import src.curt.threads_manager as tm
from src.curt.reports import loadtest
from src.curt.simple_rest import post_request, get_request
```

Deben definirse las funciones HTTP que serán utilizadas, y hacer la petición utilizando las funciones importadas `post_request` para peticiones `POST`, y `get_request` para peticiones `GET`.

Como las respuestas de cada petición se generan de forma independiente, debe existir una forma de almacenarlas. En el ejemplo se crea una lista `df_list` donde se añade cada petición realizada. El módulo `dictionary_creator` contiene una sola función, que crea un diccionario tomando la hora de inicio, de finalización, el método json utilizado y la respuesta obtenida, de forma que, uniendo todo, la parte de declaración de funciones para los métodos a consumir quedaría algo así en el `CuRT.py`:

```python
df_list = []


def post_ex():
    start = dt.datetime.now()
    # Método:
    json_method = "/posts"
    # Ubicación de la petición:
    request_file = 'resources/post.json'

    with open(request_file) as json_file:
        payload = json.load(json_file)

        # POST request
        response = post_request(p.HOST + json_method, payload, p.HEADERS)

        end = dt.datetime.now()

        df_list.append(dc.new_dict(start, end, json_method, response))


def get_ex():
    start = dt.datetime.now()
    # Método:
    json_method = "/posts/1"

    # POST request
    response = get_request(p.HOST + json_method, p.HEADERS)

    end = dt.datetime.now()

    df_list.append(dc.new_dict(start, end, json_method, response))
```

CuRT hace el consumo de los métodos del servicio web indicado y posteriormente genera un reporte con los resultados obtenidos. Como todas las herramientas para hacer esto están incluidas dentro del propio CuRT, entonces pueden utilizarse para generar reportes con base en un archivo *.csv* anterior. Por lo que en el ejemplo existen dos funciones que generan un reporte: `hacer_pruebas` y `generar_html`.

El módulo `reports.py` contiene una sola función, llamada `loadtest`, que es la que contiene la funcionalidad principal de la herramienta: hacer los reportes. Así que independientemente de cuál sea el origen de los datos, es este método el que debe ser consumido para generar el reporte.

El módulo que se encarga de hacer las pruebas de carga como tal es `threads_manager.py`. Este contiene una sola función que necesita tres valores: la duración del test en segundos, las funciones que realizan consumen el servicio web y el número de peticiones por segundo.

Uniendo estos elementos, las funciones quedan así:

```python
def hacer_pruebas(length, test_name, dark_mode, base_dir, functions, rps):
    print('## Custom RPS Tester ##')
    print('# Tester #')
    start_time = dt.datetime.now()
    tm.start_threads(int(length), functions, int(rps))
    end_time = dt.datetime.now()
    df = pd.DataFrame(df_list)
    report_dir = test_name + '_' + str(dt.datetime.now().timestamp())
    dir_name = base_dir + '/' + report_dir
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    df.to_csv(dir_name + '/data.csv', index=False, encoding='utf-8-sig')
    df.sort_values(by='End', inplace=True)
    loadtest(dark_mode=dark_mode, dir_name=dir_name, functions=functions, df=df, start_time=start_time,
             end_time=end_time, testing=True)


def generar_html(dark_mode, dir_name, csv_location):
    print('## Custom RPS Tester ##')
    print('# Generador de reportes #')
    print('##########################################################################')
    if dir_name == 'NULL':
        dir_name = os.getcwd()
    print('Cargando archivo: ' + csv_location)
    df = pd.read_csv(csv_location, encoding='utf-8')
    print('Hecho.')
    print('##########################################################################')
    function_names = df['MethodName'].drop_duplicates().tolist()
    functions = []
    for function in function_names:
        functions.append(globals()[function])
    datetime_format = '%Y-%m-%d %H:%M:%S.%f'
    start_time = dt.datetime.strptime(df['Start'].min(), datetime_format)
    end_time = dt.datetime.strptime(df['End'].max(), datetime_format)
    df.sort_values(by='End', inplace=True)
    loadtest(dark_mode=dark_mode, dir_name=dir_name, functions=functions, df=df, start_time=start_time,
             end_time=end_time, testing=False)
```

La bandera de `testing` sirve para indicar el nombre del reporte. Para un reporte nuevo, el archivo final será `index.html`. Para un reporte con base en datos previos, el archivo final será `index-[timestamp].html`.

Finalmente, la función principal definirá qué función será llamada, por lo que existen dos posibilidades:
- Se realiza la prueba de carga y entonces se genera el reporte:
```python
def main():
    length = 20
    rps = 20
    test_name = 'Ejemplo'
    base_dir = 'Reports'
    functions = [post_ex, get_ex]
    dark_mode = True
    hacer_pruebas(length=length, test_name=test_name, dark_mode=dark_mode, base_dir=base_dir, functions=functions, rps=rps)
```


- Se realiza el reporte con base en datos existentes:
```python
def main():
    dir_name = p.DIRECTORY + '/CuRT/Reports/Ejemplo_1670629990.446974'
    csv_location = dir_name + '/data.csv'
    dark_mode = True
    generar_html(dark_mode=dark_mode, dir_name=dir_name, csv_location=csv_location)
```


Finalmente se ejecuta el archivo `CuRT.py` y se genera el reporte:

![](https://i.imgur.com/64xV9ny.png)

<a name="source"></a>
## Using source code 
<a name="inst"></a>
### Instalación

#### Prerrequisitos
- Instalar Python 3.
- Agregar la ruta de instalación de Python a la variable de entorno `path`.

#### Procedimiento recomendado
- Generar un entorno virtual (`pipenv`, `virtualenv`, etc.).
- Activar el entorno virtual (el método de activación depende del módulo utilizado para esto). 
- Instalar los módulos requeridos indicados en este archivo y en el `requirements.txt`:
```bash
pip install -r requirements.txt
```


<a name="main"></a>
## Mantenimiento
Desarrolladores actuales que dan mantemiento al código:
- Jahaziel Alvarez (<a href = "mailto: jahaziel.alvarez@pm.me">jahaziel.alvarez@pm.me</a>)